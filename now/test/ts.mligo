#import "../src/contract.mligo" "MyContract"

let boot () = 
  let now : timestamp = ("1970-01-01t00:00:01Z" : timestamp) in
  let () = Test.reset_state_at now 2n ([] : tez list) in

  let (taddr, _, _) = Test.originate MyContract.main unit 0mutez in
  let contr = Test.to_contract taddr in
  let addr = Tezos.address contr in
  {addr = addr; taddr = taddr; contr = contr}

let test_e2e = 
  let c = boot() in
  let _ = Test.transfer_to_contract c.contr (Ok) 0tez in
  let _ = Test.transfer_to_contract c.contr (Ok) 0tez in
  let _ = Test.transfer_to_contract c.contr (Ok) 0tez in
  let _ = Test.transfer_to_contract c.contr (Ok) 0tez in
  let r = Test.transfer_to_contract c.contr (Nok) 0tez in
  Test.log r

let test_unit =
  let () = Test.log (MyContract.now()) in
  Test.log (MyContract.now())

let test_now = 
  Test. log (Tezos.get_now())
