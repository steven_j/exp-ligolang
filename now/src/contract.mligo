let no_operation = ([] : operation list)
type result = operation list * unit

let now () = Tezos.get_now()

type parameter = Ok | Nok

let main (p, _ : parameter * unit) : result =
  match p with 
    Ok -> no_operation, unit
    | Nok -> failwith (now())
