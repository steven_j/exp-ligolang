let test_timestamp_sub = 
  let today : timestamp = ("2001-01-01t10:10:10Z" : timestamp) in
  let some_date : timestamp = ("2000-01-01t10:10:10Z" : timestamp) in
  let diff = today - some_date in
  Test.log diff
(* ^ `diff` is a timestamp instead of an `int` *)
(* see https://ligolang.org/docs/advanced/timestamps-addresses/#subtracting-timestamps *)

let test_timestamp_sub_with_cast = 
  let today : timestamp = ("2001-01-01t10:10:10Z" : timestamp) in
  let some_date : timestamp = ("2000-01-01t10:10:10Z" : timestamp) in
  let diff = today - some_date in
  Test.log (int(diff))
(* File "src/passes/14-spilling/compiler.ml", line 92, characters 43-50 *)
(*  corner case: invalid external_int application *)
(* Sorry, we don't have a proper error message for this error. Please report this use case so we can improve on this. *)
