# timestamp errors in test framework

Run `make` to launch tests

```sh
➜ make 
File "src/passes/14-spilling/compiler.ml", line 92, characters 43-50
 corner case: invalid external_int application
Sorry, we don't have a proper error message for this error. Please report this use case so we can improve on this.
timestamp(1940-01-01T13:49:50Z)
make: *** [Makefile:11 : test] Erreur 1
```
