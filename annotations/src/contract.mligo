let no_operation = ([] : operation list)
type storage = unit
type result = operation list * storage

type token_id = nat
type token_addr = address

type foo = {
  token_id: nat;
  token_addr: address;
}

type bar = {
  token_id: token_id;
  token_addr: token_addr;
}

type parameter =
  Foo of foo
  | Bar of bar

let main (action, s : parameter * storage) : result =
  match action with
  Foo _ -> no_operation, s
  | Bar _ -> no_operation, s
