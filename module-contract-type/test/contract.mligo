#import "../src/contract.mligo" "Contract"

let boot () =
  let (taddr, _, _) = Test.originate_module (contract_of Contract) 0 0mutez in
  let contr = Test.to_contract taddr in
  let addr = Tezos.address contr in
  {addr = addr; taddr = taddr; contr = contr}

let how_to_type (c: contract) =
  Test.log c

let test_timestamp =
  let c = boot() in
  how_to_type c.contr
