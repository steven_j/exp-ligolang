# Module Contract Type

Run `make test` to see the error:

```sh
➜ make test
File "./test/contract.mligo", line 14, characters 14-21:
 13 |   let c = boot() in
 14 |   how_to_type c.contr

Invalid type(s)
Cannot unify "contract (Contract.parameter#678)" with "contract".
make: *** [Makefile:17 : test] Erreur 1
```
