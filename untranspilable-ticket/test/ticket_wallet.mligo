#import "../src/ticket_wallet.mligo" "Tckt"

(* let () = Test.reset_state 2n ([] : tez list) *)
(* let sender_ = Test.nth_bootstrap_account 1 *)

(* let init_storage = { *)
(*     manager = sender_; *)
(*     tickets = (Big_map.empty: (address, unit ticket) big_map); *)
(* } *)

(* let (taddr, _, _) = Test.originate Tckt.main init_storage 0mutez *)
(* let contr = Test.to_contract taddr *)
(* let addr = Tezos.address contr *)

(** 
  The code above gives a warning that we don't reproduce when wrapping in a
  function:
  
  Warning: variable "init_storage" cannot be used more than once. 
**)

let boot () = 
  let () = Test.reset_state 2n ([] : tez list) in
  let sender_ = Test.nth_bootstrap_account 1 in
  let () = Test.set_source sender_ in

  let init_storage = {
      data = {
        price = 1tz;
      };
      tickets = (Big_map.empty: (address, unit ticket) big_map);
  } in

  let (taddr, _, _) = Test.originate Tckt.main init_storage 0mutez in
  let contr = Test.to_contract taddr in
  let addr = Tezos.address contr in
  {addr = addr; taddr = taddr; contr = contr}

let test_one = 
  let tckt = boot() in
  let r = Test.transfer_to_contract tckt.contr () 1tez in
  Test.log r
