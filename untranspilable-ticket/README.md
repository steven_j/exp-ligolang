# Untranspilable ticket

Run `make test` to see the error:

```sh
➜ make test

untranspilable (ticket unit) (Pair 0x01dcd96706b367551b7dbcb5e5da49c59e6885ff9000 (Pair Unit 1))
```
