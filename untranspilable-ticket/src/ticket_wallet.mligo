module Constants =
  struct
    let no_operation : operation list = []
    let fixed_amount = 1n
  end

module Errors = 
  struct
    let cannot_join_tickets = "CANNOT_JOIN_TICKETS"
    let wrong_amount_given = "WRONG_AMOUNT_GIVEN"
  end

module Storage =
  struct
    type tickets = (address, unit ticket) big_map
    type storage_data = {
        price : tez;
    }

    type t = {data : storage_data; tickets : tickets}
  end

type storage = Storage.t
type result = operation list * storage

let buy_ticket ({data; tickets} : storage) : result =
  (* expecting only one ticket purchase *)
  let _check_given_amount =
    assert_with_error
      (Tezos.get_amount () = data.price)
      Errors.wrong_amount_given in

  let owner = (Tezos.get_sender()) in

  let (owned_tickets_opt, tickets) =
    Big_map.get_and_update owner (None : unit ticket option) tickets in

  let new_ticket = Tezos.create_ticket unit Constants.fixed_amount in

  let join_tickets =
    match owned_tickets_opt with
      None -> new_ticket
    | Some owned_tickets ->
        (match Tezos.join_tickets (owned_tickets, new_ticket) with
            None -> failwith Errors.cannot_join_tickets
            | Some joined_tickets -> joined_tickets)
  in

  let (_, tickets) =
    Big_map.get_and_update owner (Some join_tickets) tickets in
  Constants.no_operation, {data = data; tickets = tickets}

let main (_, store : unit * storage) : result =
  buy_ticket(store)
