let no_operation = ([] : operation list)
type storage = timestamp
type result = operation list * storage

let main (ts, _ : timestamp * storage) : result =
  no_operation, ts 
