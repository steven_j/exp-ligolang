let no_operation = ([] : operation list)

type storage = unit

type result = operation list * storage

type parameter = Default of unit | Withdraw of address

let withdraw (receiver_addr: address) =
  let receiver : unit contract =
    match (Tezos.get_contract_opt receiver_addr
           : unit contract option)
    with
      Some (contract) -> contract
    | None -> (failwith "Not a contract" : unit contract) in
  let tx =
    Tezos.transaction
      ()
      (Tezos.get_balance ())
      receiver in
  [tx], unit

let main (action, store : parameter * storage) : result =
  match action with
    Default _ -> no_operation, store
  | Withdraw addr -> withdraw addr
