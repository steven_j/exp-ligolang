#import "../src/contract.mligo" "Contract"

let test_default =
  let addr =
    Test.originate_contract
      (Test.compile_contract Contract.main)
      (Test.eval ())
      0mutez in
  let taddr =
    (Test.cast_address addr
     : (Contract.parameter, Contract.storage) typed_address) in
  let contr = Test.to_entrypoint "default" taddr in
  let _ = Test.transfer_to_contract_exn contr () 30mutez in
  assert (Test.get_balance (addr) = 30mutez)

let test_withdraw =
  let () = Test.reset_state 2n [10_000tez; 1tez] in
  let baker = Test.nth_bootstrap_account 0 in
  let receiver = Test.nth_bootstrap_account 1 in

  let balance_before = Test.get_balance (receiver) in
  let () = Test.log "Balance before:" in
  let () = Test.log balance_before in

  (* Originate contract *)
  let () = Test.set_source baker in
  let addr =
    Test.originate_contract
      (Test.compile_contract Contract.main)
      (Test.eval ())
      3tez in
  let taddr =
    (Test.cast_address addr
     : (Contract.parameter, Contract.storage) typed_address) in
  let () = Test.log "Balance contract:" in
  let () = Test.log (Test.get_balance (addr)) in

  (* Call withdraw *)
  let () = Test.set_source receiver in
  let contr = Test.to_entrypoint "withdraw" taddr in
  let cost = Test.transfer_to_contract_exn contr (receiver) 0mutez in
  let () = Test.log "Gas:" in
  let () = Test.log cost in
  let balance_after = Test.get_balance (receiver) in
  let () = Test.log "Balance after:" in
  (* let () = Test.log balance_after in *)
  Test.log balance_after
  (* let diff = Option.unopt (balance_after - balance_before) in *)
  (* let () = Test.log "Balance contract:" in *)
  (* let () = Test.log (Test.get_balance (addr)) in *)
  (* let () = Test.log "Balance diff:" in *)
  (* Test.log diff *)
